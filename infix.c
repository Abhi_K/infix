#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#define OPERAND 1
#define OPERATOR 2
#define	END 3
#define ERROR 4
#include "stack.h"
int infixval(char *infix);
int readline(char *infix, int len) {
	int i = 0;
	int ch;
	while((ch = getchar()) != '\n' && i < len - 1) {
		infix[i] = ch;
	}
	infix[i] = '\0';
	return i;
}
typedef struct token{
	int number;
	char op;
	int type;
}token; 
enum states { SPC, DIG, OPR, BRK, STOP, ERR };
token *getnext(char *arr) {
	static int currstate = SPC;
	int nextstate;
	static int i = 0;
	int num;
	token *t = (token *)malloc(sizeof(token));
	while(1) {
		switch(arr[i]) {
			case '0': case '1': case '2': case '3':
			case '4': case '5': case '6': case '7':
			case '8': case '9': 
				nextstate = DIG;
				break;
			case  '+': case '-': case '*': case '/':
			case '%': case '(': case ')':
				nextstate = OPR;
				break;
			case '\0':
				nextstate = STOP;
				break;
			case ' ':
				nextstate = SPC;
				break;
			default:
				nextstate = ERR;
				break;
		}
		switch(currstate) {
			case SPC:
				if(nextstate == DIG)
						num = arr[i] - '0';
				break;
			case DIG:
				if(nextstate == DIG)
					num = num * 10 + arr[i] - '0';
				else  {
					t->type = OPERAND;
					t->number = num;
					currstate = nextstate;
					return t;
				}
				break;
			case OPR:
				t->type = OPERATOR;
				t->op = arr[i-1];
				currstate = nextstate;
				return t;
				break;
			case STOP:
				t->type = END;
				currstate = nextstate;
				return t;
				break;
			case ERR:
				t->type = ERROR;
				currstate = nextstate;
				return t;
				break;
		}
		currstate = nextstate;
		i++;
	}
}

int prec(char c){
	if(c == '(')
		return 4;
	if(c == '*' || c == '/')
		return 3;
	if(c == '+' || c == '-')
		return 2;
	if(c == ')')
		return 1;
	else
		return 0;
}
int infixval(char *infix){
	token *t;
	int x, y,c=0;
	char o;
	int result;
	stack a;
	stack1 b;
	init(&a);
	init1(&b);
	while(1){
		t=getnext(infix);
		if(t->type == OPERAND)
			push(&a, t->number);
		else if(t->type == OPERATOR){
			if(!empty1(&b)){
				o = pop1(&b);
				if(prec(o) >= prec(t->op)){
						if(!empty(&a))
							x = pop(&a);
						else
							return INT_MIN; 
						if(!empty(&a))
							y = pop(&a);
						else
							return INT_MIN; 
					switch(o){
						case '*' :
							result = y*x;
							push(&a,result);
							break;
						case '/' :
							result = y/x;
							push(&a,result);
							break;
						case '+' :
							result = y+x;
							push(&a,result);
							break;
						case '-' : 
						result = y-x;
							push(&a,result);
							break;
							case '(' :
							push(&a, y);
							push(&a, x);
							break;
					}
				}
				else if(o!=')')
					push1(&b, o);	
				push1(&b, t->op);
			}
			else
				push1(&b, t->op);
		}
		else if (t->type == ERROR) 
			return INT_MIN; 
		else {
			while(!empty1(&b)){
				if(!empty(&a))
					x = pop(&a);
				else
					return INT_MIN; 
				if(!empty(&a))
					y = pop(&a);
				else
					return INT_MIN; 
				o = pop1(&b);
				switch(o){
					case '*' :
						result = y*x;
						break;
					case '/' :
						result = y/x;
						break;
					case '+' :
						result = y+x;
						break;
					case '-' : 
						result = y-x;
						break;
				}
				push(&a,result);
			}
			return result;
		}
	}
}
int main() {
	char infix[128];
	int ans;
	readline(infix, 128);
	ans = infixval(infix);
	if(ans == INT_MIN)
		printf("Erorr in expression\n");
	else
		printf("%d\n", ans);
	return 0;
}

